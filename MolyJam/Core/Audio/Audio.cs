﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;

namespace MolyJam.Core.Audio
{
    public enum AudioFile {
        AUDIO_BODYHIT = 0,
        AUDIO_CAR,
        AUDIO_SAVED,
        AUDIO_AMBIENT,
        AUDIO_MAIN_MENU
    }


    public sealed class AudioSingleton
    {
        private static AudioSingleton m_AudioSingleton = null;
        private Dictionary<AudioFile, SoundEffectInstance> m_SoundList = null;

        public static AudioSingleton GetInstance() {
            if(m_AudioSingleton == null) {
                m_AudioSingleton = new AudioSingleton();
            }
            return m_AudioSingleton;
        }

        public void Initialize() {
            if(m_SoundList == null) {
                m_SoundList = new Dictionary<AudioFile, SoundEffectInstance>();
            }
        }

        public void AddTrack(AudioFile audioType, SoundEffect soundFile) {
            if(m_SoundList.ContainsKey(audioType)) {
                return;
            }
            SoundEffectInstance tempSoundInstance = soundFile.CreateInstance();
            m_SoundList.Add(audioType, tempSoundInstance);
        }
        
        public void PlayMainMenuAmbient() {
            m_SoundList[AudioFile.AUDIO_MAIN_MENU].IsLooped = true;
            m_SoundList[AudioFile.AUDIO_MAIN_MENU].Play();
        }

        public void StopMainMenuAmbient()
        {
            m_SoundList[AudioFile.AUDIO_MAIN_MENU].Stop();
        }

        public void PlayAmbient() {
            m_SoundList[AudioFile.AUDIO_AMBIENT].IsLooped = true;
            m_SoundList[AudioFile.AUDIO_AMBIENT].Play();
            m_SoundList[AudioFile.AUDIO_CAR].IsLooped = true;
           // m_SoundList[AudioFile.AUDIO_CAR].Play();
        }

        public void StopAmbient() {
            m_SoundList[AudioFile.AUDIO_AMBIENT].Stop();
        }


        public void PlayOnce(AudioFile audioType)
        {
            if(m_SoundList.ContainsKey(audioType)) {
                m_SoundList[audioType].Play();
            } else {
                Console.WriteLine("No sound file found nr." + audioType);
            }
        }


        private AudioSingleton()
        {
            //private constructor
        }
    }
}
