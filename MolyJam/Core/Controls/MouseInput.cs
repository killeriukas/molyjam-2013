﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTS.Framework.InputControl;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RTS.Core.Controls
{
    public class MouseInput : IAimSystem
    {
        private MouseState m_OldMouseState;
        int m_mouseX, m_mouseY;
        int m_mouseDeltaX, m_mouseDeltaY;

        public MouseInput() {
           // m_OldMouseState = Mouse.GetState();
            m_mouseDeltaX = 0;
            m_mouseDeltaY = 0;
        }

        public Vector2 GetPointerLocation()
        {
            return new Vector2(m_mouseX, m_mouseY);
        }

        public Vector2 GetPointerDeltas()
        {
            return new Vector2(m_mouseDeltaX, m_mouseDeltaY);
        }

        public void Update(int deltaTimeMS)
        {
            //take new state of the mouse
            MouseState newMouseState = Mouse.GetState();
            m_mouseX = newMouseState.X;
            m_mouseY = newMouseState.Y;

            //if last frame mouse position is equal to current frame mouse position, set both deltas to 0
            if (m_mouseX == m_OldMouseState.X && m_mouseY == m_OldMouseState.Y) {
                m_mouseDeltaX = 0;
                m_mouseDeltaY = 0;
            } else { //if mouse position is different, calculate the delta position
                m_mouseDeltaX = m_mouseX - m_OldMouseState.X;
                m_mouseDeltaY = m_mouseY - m_OldMouseState.Y;
            }

            m_OldMouseState = newMouseState;
        }

        public bool IsMouseLeftButtonPressedOnce()
        {

            if(Mouse.GetState().LeftButton == ButtonState.Pressed && m_OldMouseState.LeftButton == ButtonState.Released) {
                return true;
            }

            return false;
        }

        public bool IsMouseLeftButtonPressed()
        {

            if(Mouse.GetState().LeftButton == ButtonState.Pressed) {
                return true;
            }

            return false;
        }


        public bool IsMouseRightButtonPressedOnce()
        {
            if (Mouse.GetState().RightButton == ButtonState.Pressed && m_OldMouseState.RightButton == ButtonState.Released)
            {
                return true;
            }

            return false;
        }

        public bool IsMouseRightButtonPressed()
        {
            if (Mouse.GetState().RightButton == ButtonState.Pressed)
            {
                return true;
            }

            return false;
        }

    }
}
