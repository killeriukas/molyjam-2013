﻿using RTS.Framework.SceneManager;
using Microsoft.Xna.Framework.Content;
using MolyJam.Core.GameScenes.Mesh;
using Microsoft.Xna.Framework.Graphics;
using MolyJam.Core.GameScenes.Map;
using System.Collections.Generic;
using MolyJam.Core.GameScenes.Mesh.People;
using MolyJam.Core.GameScenes.Physics;
using MolyJam.Core.Audio;
using Microsoft.Xna.Framework.Audio;

namespace MolyJam.Core.GameScenes
{
    public class GamePlayScene : AScene
    {
        private Hearse m_Hearse;
        private MapClass m_Map;
        private List<APeople> m_People;
        private PhysicsClass m_Physics;
        private Player m_Player;
        private int m_SpawnTimer;

        public GamePlayScene(int windowX, int windowY, ContentManager contentManager)
            : base("GamePlayScene", windowX, windowY, contentManager)
        {
            m_Hearse = null;
            m_Map = null;
            m_People = null;
            m_Physics = null;
            m_Player = null;
        }


        public override MyResult Start()
        {
            m_SpawnTimer = 0;

            m_Map = new MapClass(m_ContentManager);
            if(m_Map == null) {
                return MyResult.MY_RESULT_CRITICAL_ERROR;
            }

            m_People = new List<APeople>();
            if (m_People == null)
            {
                return MyResult.MY_RESULT_CRITICAL_ERROR;
            }

            m_Hearse = new Hearse(m_ContentManager);
            if(m_Hearse == null) {
                return MyResult.MY_RESULT_CRITICAL_ERROR;
            }

            m_Physics = new PhysicsClass();
            if (m_Physics == null)
            {
                return MyResult.MY_RESULT_CRITICAL_ERROR;
            }

            m_Player = new Player(m_ContentManager);
            if (m_Player == null)
            {
                return MyResult.MY_RESULT_CRITICAL_ERROR;
            }

            AudioSingleton.GetInstance().Initialize();
            AudioSingleton.GetInstance().AddTrack(AudioFile.AUDIO_AMBIENT, m_ContentManager.Load<SoundEffect>("Audio/Music/Ambient"));
            AudioSingleton.GetInstance().AddTrack(AudioFile.AUDIO_CAR, m_ContentManager.Load<SoundEffect>("Audio/SFX/car"));
            AudioSingleton.GetInstance().AddTrack(AudioFile.AUDIO_BODYHIT, m_ContentManager.Load<SoundEffect>("Audio/SFX/bodyhit"));
            AudioSingleton.GetInstance().AddTrack(AudioFile.AUDIO_SAVED, m_ContentManager.Load<SoundEffect>("Audio/SFX/saved"));
            AudioSingleton.GetInstance().PlayAmbient();

            //AudioSingleton.GetInstance().

            return MyResult.MY_RESULT_SUCCESSFUL;
        }

        public override void Stop()
        {
            m_Hearse = null;
            m_Map = null;
            //for() {
                
            //}
            m_People.Clear();
            m_People = null;
            m_Physics.Clear();
            m_Physics = null;
            m_Player = null;
            m_ContentManager.Unload();

        }

        public override MyResult UpdateInput(int deltaTimeMS, RTS.Framework.InputControl.InputHandler inputHandler)
        {
            for (int i = 0; i < m_People.Count; ++i)
            {
                m_People[i].UpdateInput(deltaTimeMS, inputHandler);
            }


            return MyResult.MY_RESULT_SUCCESSFUL;
        }


        public override MyResult UpdateGameLogic(int deltaTimeMS)
        {
            MyResult result = MyResult.MY_RESULT_SUCCESSFUL;

            m_SpawnTimer += deltaTimeMS;

            m_Hearse.UpdateLogic(deltaTimeMS);

            if (m_People.Count < 3 && m_SpawnTimer > 3000)
            {
                m_SpawnTimer -= 3000;
                APeople person = new OldGranies(m_ContentManager, WINDOW_WIDTH);
                m_People.Add(person);
                m_Physics.Add(person);
            }

            result = m_Map.UpdateGameLogic(deltaTimeMS);

            for (int i = 0; i < m_People.Count; ++i)
            {
                m_People[i].UpdateGameLogic(deltaTimeMS);
            }

            m_Physics.RunSimulation(deltaTimeMS);

            for (int i = 0; i < m_People.Count; ++i)
            {
                //check whether the person is alive
                if(!m_People[i].IsAlive()) {
                    //m_Player.IncreaseDeadLifes();
                   // m_Hearse.AddDeadBody();
                    if(!m_People[i].IgnoreCollision()) {
                        AudioSingleton.GetInstance().PlayOnce(AudioFile.AUDIO_BODYHIT);
                    }
                    m_People[i].Spawn();
                    continue;
                }

                //check whether person landed on the hearse
                if (m_Hearse.IsInBounds(m_People[i].GetPosition(), m_People[i].GetBounds()))
                {
                    m_Player.IncreaseSavedLifes();
                    AudioSingleton.GetInstance().PlayOnce(AudioFile.AUDIO_SAVED);
                    m_People[i].Spawn();
                    continue;
                }

                //check whether the person hit the car
                if (!m_People[i].IgnoreCollision() && m_Hearse.HasHitPlayer(m_People[i].GetPosition(), m_People[i].GetBounds()))
                {
                    m_Player.IncreaseDeadLifes();
                    m_Hearse.AddDeadBody();
                    AudioSingleton.GetInstance().PlayOnce(AudioFile.AUDIO_BODYHIT);
                    m_People[i].Spawn();
                    continue;
                }


            }

            //if more than dead bodies, move hearse forward
            const int PEOPLE_NUMBER_TO_KILL = 3;
            int vansCount = m_Hearse.GetVansAmount();
            int deadBodiesCountInVans = m_Player.GetDeadBodies() / PEOPLE_NUMBER_TO_KILL;

            if(deadBodiesCountInVans > vansCount) {
                m_Hearse.AddVan();
            }

            m_Hearse.MoveForward(deltaTimeMS);

            const int LOSE_NUMBER = 2;

            if (m_Hearse.GetVansAmount() > LOSE_NUMBER)
            {
                AudioSingleton.GetInstance().StopAmbient();
                ChangeScene("GameOverScene");
            }

            const int WIN_NUMBER = 9;
            if (m_Player.GetSavedSouls() > WIN_NUMBER)
            {
                AudioSingleton.GetInstance().StopAmbient();
                ChangeScene("GameWinScene");
            }

            return result;
        }

        public override void Render(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            m_Map.Render(spriteBatch);
            m_Hearse.Render(spriteBatch);
            for (int i = 0; i < m_People.Count; ++i)
            {
                m_People[i].Render(spriteBatch);
            }

            m_Player.Render(spriteBatch);

            spriteBatch.End();
        }
    }
}
