﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTS.Framework.SceneManager;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace MolyJam.Core.GameScenes
{
    public class GameWinScene : AScene
    {
        private Texture2D m_Background;

        public GameWinScene(int windowX, int windowY, ContentManager contentManager)
            : base("GameWinScene", windowX, windowY, contentManager) 
        {
            m_Background = null;
        }


        public override MyResult Start()
        {
            m_Background = m_ContentManager.Load<Texture2D>("Map/win_screen");
            if (m_Background == null)
            {
                return MyResult.MY_RESULT_CRITICAL_ERROR;
            }

            return MyResult.MY_RESULT_SUCCESSFUL;
        }

        public override void Stop()
        {
            m_ContentManager.Unload();
        }

        public override MyResult UpdateInput(int deltaTimeMS, RTS.Framework.InputControl.InputHandler inputHandler)
        {
            if (inputHandler.AimSystem.IsMouseLeftButtonPressed())
            {
                ChangeScene("GamePlayScene");
            }

            return MyResult.MY_RESULT_SUCCESSFUL;
        }

        public override MyResult UpdateGameLogic(int deltaTimeMS)
        {
            return MyResult.MY_RESULT_SUCCESSFUL;
        }

        public override void Render(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(m_Background, Vector2.Zero, Color.White);
            spriteBatch.End();
        }
    }
}
