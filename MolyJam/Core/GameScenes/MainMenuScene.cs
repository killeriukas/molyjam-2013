﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using RTS.Framework.SceneManager;
using Microsoft.Xna.Framework.Graphics;
using MolyJam.Core.Audio;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace MolyJam.Core.GameScenes
{
    public class MainMenuScene : AScene
    {

        private Texture2D m_Background;

        public MainMenuScene(int windowX, int windowY, ContentManager contentManager)
            : base("MainMenuScene", windowX, windowY, contentManager)
        {
            m_Background = null;
        }

        public override MyResult Start()
        {
            m_Background = m_ContentManager.Load<Texture2D>("Map/start_screen");
            AudioSingleton.GetInstance().Initialize();
            AudioSingleton.GetInstance().AddTrack(AudioFile.AUDIO_MAIN_MENU, m_ContentManager.Load<SoundEffect>("Audio/Music/MainMenuAmbient"));
            AudioSingleton.GetInstance().PlayMainMenuAmbient();

            return MyResult.MY_RESULT_SUCCESSFUL;
        }

        public override void Stop()
        {
            m_ContentManager.Unload();
            
        }

        public override MyResult UpdateInput(int deltaTimeMS, RTS.Framework.InputControl.InputHandler inputHandler)
        {
            if(inputHandler.AimSystem.IsMouseLeftButtonPressed()) {
                AudioSingleton.GetInstance().StopMainMenuAmbient();
                ChangeScene("GamePlayScene");
            }

            return MyResult.MY_RESULT_SUCCESSFUL;
        }

        public override MyResult UpdateGameLogic(int deltaTimeMS)
        {


            return MyResult.MY_RESULT_SUCCESSFUL;
        }

        public override void Render(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(m_Background, Vector2.Zero, Color.White);
            spriteBatch.End();
        }
    }
}
