﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using RTS.Framework.SceneManager;

namespace MolyJam.Core.GameScenes.Map
{
    public class Background
    {
        //private Texture2D m_MainBackground;
        private Texture2D[,] m_Background;
        private Vector2[,] m_Position;
        //private Vector2 m_MainPosition;

        public Background(ContentManager contentManager) {

            //m_MainBackground = contentManager.Load<Texture2D>("Map/MainBackground");
            //m_MainPosition = Vector2.Zero;

            m_Background = new Texture2D[3,3];
            m_Background[0, 0] = contentManager.Load<Texture2D>("Map/MainBackground0");
            m_Background[0, 1] = contentManager.Load<Texture2D>("Map/MainBackground1");
            m_Background[0, 2] = contentManager.Load<Texture2D>("Map/MainBackground2");
            m_Background[1, 0] = contentManager.Load<Texture2D>("Map/MiddleLayer0");
            m_Background[1, 1] = contentManager.Load<Texture2D>("Map/MiddleLayer1");
            m_Background[1, 2] = contentManager.Load<Texture2D>("Map/MiddleLayer2");
            m_Background[2, 0] = contentManager.Load<Texture2D>("Map/BgrStreet0");
            m_Background[2, 1] = contentManager.Load<Texture2D>("Map/BgrStreet1");
            m_Background[2, 2] = contentManager.Load<Texture2D>("Map/BgrStreet2");

            m_Position = new Vector2[3,3];

            m_Position[0, 0] = Vector2.Zero;
            m_Position[0, 1].X += m_Background[0, 0].Width;
            m_Position[0, 2].X += m_Background[0, 0].Width * 2;

            //nearest layer
            m_Position[1,0] = Vector2.Zero;
            m_Position[1,1].X += m_Background[1,0].Width;
            m_Position[1,2].X += m_Background[1,0].Width * 2;

            //middle layer
            m_Position[2, 0] = Vector2.Zero;
            m_Position[2, 1].X += m_Background[2, 0].Width;
            m_Position[2, 2].X += m_Background[2, 0].Width * 2;


        }


        public MyResult UpdateGameLogic(int deltaTimeMS)
        {
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    if (m_Position[i,j].X > -m_Background[i,j].Width)
                    {
                        m_Position[i, j].X -= deltaTimeMS * 0.15f * (i+1);
                    }
                    else
                    {
                        m_Position[i,j].X += m_Background[i,j].Width * 3 - 10;
                    }
                }
            }

            return MyResult.MY_RESULT_SUCCESSFUL;
        }



        public void Render(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(m_MainBackground, m_MainPosition, Color.White);

            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    if (m_Position[i,j].X < m_Background[i,j].Width || m_Position[i,j].X < m_Background[i,j].Width)
                    {
                        spriteBatch.Draw(m_Background[i,j], m_Position[i,j], Color.White);
                    }
                }
            }
        }




    }
}
