﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using RTS.Framework.SceneManager;

namespace MolyJam.Core.GameScenes.Map
{
    public class MapClass
    {
        private Background m_Background;


        public MapClass(ContentManager contentManager)
        {
            m_Background = new Background(contentManager);
        }

        public MyResult UpdateGameLogic(int deltaTimeMS)
        {
            MyResult result = MyResult.MY_RESULT_SUCCESSFUL;

            result = m_Background.UpdateGameLogic(deltaTimeMS);

            return result;
        }

        public void Render(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            m_Background.Render(spriteBatch);
        }

    }
}
