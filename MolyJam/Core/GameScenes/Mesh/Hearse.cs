﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace MolyJam.Core.GameScenes.Mesh
{

    public class Hearse
    {
        private Texture2D[] m_Head;           //main collision body
        private Vector2 m_Position;         //position of the head on the screen
        private Vector2 m_Bounds;
        private int m_VansAmount;
        private int m_DeadBodiesAmountPerVan;
        private int m_VanAdditionTime;
        private Vector2 m_MoveToPosition;
        private bool m_isVanMoving;
        private List<Van> m_Van;
        private ContentManager m_ContentManager;
        private int m_spriteNumber;
        private int m_spriteChangeCounter;

        public Hearse(ContentManager contentManager)
        {
            m_Position = new Vector2(0, 300);
            m_ContentManager = contentManager;
            m_Head = new Texture2D[3];
            for (int i = 0; i < m_Head.Length; ++i)
            {
                m_Head[i] = contentManager.Load<Texture2D>("Mesh/HearseCar" + i);
            }
           
            m_Bounds = new Vector2(m_Head[0].Width, m_Head[0].Height);
            m_VansAmount = 0;
            m_VanAdditionTime = 0;
            m_MoveToPosition = m_Position;
            m_isVanMoving = false;
            m_Van = new List<Van>();
            m_DeadBodiesAmountPerVan = 0;
            m_spriteNumber = 0;
            m_spriteChangeCounter = 0;
        }

        public bool IsInBounds(Vector2 curPosition/*top left*/, Vector2 targetBounds) {
            //if (curPosition.X < m_Position.X + m_Bounds.X && curPosition.X > m_Position.X && curPosition.Y > m_Position.Y && curPosition.Y < m_Position.Y + m_Bounds.Y)
            //{
            //    return true;
            //}


            if (curPosition.X < m_Position.X + m_Bounds.X - 10 && curPosition.X > m_Position.X && curPosition.Y < m_Position.Y && curPosition.Y + targetBounds.Y > m_Position.Y)
            {
                return true;
            }

            return false;
        }

        public bool HasHitPlayer(Vector2 curPosition/*top left*/, Vector2 targetBounds)
        {
            if (curPosition.X < m_Position.X + m_Bounds.X && curPosition.X > m_Position.X && curPosition.Y < m_Position.Y && curPosition.Y + targetBounds.Y > m_Position.Y)
            {
                return true;
            }

            if (curPosition.X < m_Position.X + m_Bounds.X && curPosition.X > m_Position.X && curPosition.Y > m_Position.Y && curPosition.Y < m_Position.Y + m_Bounds.Y)
            {
                return true;
            }

            return false;
        }

        public void AddVan() {
            ++m_VansAmount;
            m_MoveToPosition.X += 128;
            m_isVanMoving = true;
            Texture2D[] vanTexture = new Texture2D[2];
            vanTexture[0] = m_ContentManager.Load<Texture2D>("Mesh/HearseVan00");
            vanTexture[1] = m_ContentManager.Load<Texture2D>("Mesh/HearseVan01");
            m_Van.Add(new Van(m_Position.Y, vanTexture, m_VansAmount));
        }

        public int GetVansAmount() {
            return m_VansAmount;
        }

        public void AddDeadBody() {
            ++m_DeadBodiesAmountPerVan;
            if (m_DeadBodiesAmountPerVan > 3 && m_DeadBodiesAmountPerVan % 3 != 0)
            {
                Texture2D[] vanTexture = new Texture2D[2];
                int spriteSheetNumber = m_DeadBodiesAmountPerVan % 3;
                vanTexture[0] = m_ContentManager.Load<Texture2D>("Mesh/HearseVan" + spriteSheetNumber + "0");
                vanTexture[1] = m_ContentManager.Load<Texture2D>("Mesh/HearseVan" + spriteSheetNumber + "1");
                m_Van[m_VansAmount - 1].ChangeSpriteSheet(vanTexture);
            }
        }

        public void Render(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {
            //spriteBatch.Draw(m_Head, m_Position, Color.White);
            int drawVan = m_DeadBodiesAmountPerVan;
            if(drawVan > 2) {
                drawVan = 2;
            }

            m_spriteNumber %= 2;
            
            spriteBatch.Draw(m_Head[drawVan], m_Position, new Rectangle(0, 128 * m_spriteNumber, 256, 128), Color.White);

            if (m_spriteChangeCounter > 100)
            {
                m_spriteChangeCounter -= 100;
                ++m_spriteNumber;
            }
            

            for (int i = 0; i < m_Van.Count; ++i)
            {
                m_Van[i].Render(spriteBatch);
            }
        }

        public Vector2 GetPosition() {
            return m_Position;
        }

        public void MoveForward(int deltaTimeMS) {
            if (m_isVanMoving)
            {
                m_VanAdditionTime += deltaTimeMS;
                Vector2 moveBy = Vector2.Lerp(m_Position, m_MoveToPosition, m_VanAdditionTime / 2000.0f);

                m_Position = moveBy;
                for (int i = 0; i < m_Van.Count; ++i)
                {
                    m_Van[i].MoveBy(moveBy.X);
                }

                if (m_VanAdditionTime > 2000)
                {
                    m_VanAdditionTime -= 2000;
                    m_isVanMoving = false;
                }
            }
            


        }

        public void UpdateLogic(int deltaTimeMS)
        {
            m_spriteChangeCounter += deltaTimeMS;
            for (int i = 0; i < m_Van.Count; ++i)
            {
                m_Van[i].UpdateLogic(deltaTimeMS);
            }
        }
    }
}
