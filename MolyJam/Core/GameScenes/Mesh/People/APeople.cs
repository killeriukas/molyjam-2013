﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using MolyJam.Core.GameScenes.Physics;
using System;
using Microsoft.Xna.Framework.Content;
using MolyJam.Core.Audio;

namespace MolyJam.Core.GameScenes.Mesh.People
{
    public enum PeopleState {
        PEOPLE_IDLE = 0,
        PEOPLE_FLYING = 1,
        PEOPLE_DEAD = 2
    }

    public abstract class APeople : APhysicObject
    {
        protected Texture2D[] m_PersonImage;
        private PeopleState m_State;
        private bool m_IsLaunchActivated;
        protected Vector2 m_Bounds;
        private Vector2 m_mouseStartPos;
        private Vector2 m_mouseEndPos;
        private Vector2 m_launchDirection;
        private bool m_mouseReleased;
        protected bool m_isAlive;
        protected Random m_Random;
        private float m_randRotation;
        protected Vector2 m_PersonOrigin;

        static float CIRCLE_LENGTH = MathHelper.Pi * 2;

        public APeople(ContentManager contentManager)
        {
            m_Random = new Random();
            Initialize();
        }

        protected virtual void Initialize()
        {
            m_State = PeopleState.PEOPLE_IDLE;
            m_IsLaunchActivated = false;
            m_launchDirection = Vector2.Zero;
            m_mouseReleased = true;
            m_isAlive = true;
        }

        public void Render(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
           // spriteBatch.Draw(m_PersonImage, m_Position, Color.White);
            spriteBatch.Draw(m_PersonImage[(int)m_State], m_Position, null, Color.White, m_Rotation, Vector2.Zero, 1.0f, SpriteEffects.None, 0);
        }


        public override void UpdatePhysics(int deltaTimeMS)
        {
            //if people are idle, there is no need to affect them with physics
            if(m_State != PeopleState.PEOPLE_FLYING) {
                return;
            }

            ApplyLinearForce(m_launchDirection);



            ApplyAngularForce((float)(deltaTimeMS / 1000.0f) % CIRCLE_LENGTH);

            m_launchDirection = Vector2.Zero;

            base.UpdatePhysics(deltaTimeMS);

            //if person dropped on the ground, make it idle
            if (m_Position.Y > 400)
            {
                m_State = PeopleState.PEOPLE_DEAD;
                m_Rotation = 0;
                AudioSingleton.GetInstance().PlayOnce(AudioFile.AUDIO_BODYHIT);
                //m_isAlive = false;
            }

        }

        private bool IsInBounds(Vector2 mousePos) {
            if(m_State == PeopleState.PEOPLE_DEAD) {
                return false;
            }

            if (mousePos.X < m_Position.X + m_Bounds.X && mousePos.X > m_Position.X && mousePos.Y > m_Position.Y && mousePos.Y < m_Position.Y + m_Bounds.Y)
            {
                return true;
            }
            return false;
        }

        public void UpdateInput(int deltaTimeMS, RTS.Framework.InputControl.InputHandler inputHandler)
        {
            //m_MouseStillPressed = false;

            //update input only when person is in idle state and pointer is on the person
            if(m_State == PeopleState.PEOPLE_IDLE && IsInBounds(inputHandler.AimSystem.GetPointerLocation())) {
                if (inputHandler.AimSystem.IsMouseLeftButtonPressed())
                {
                    m_IsLaunchActivated = true;
                    m_mouseReleased = false;
                    m_mouseStartPos = inputHandler.AimSystem.GetPointerLocation();
                }
            }
            
            //check if launch has been activated and mouse button has been released
            if (!inputHandler.AimSystem.IsMouseLeftButtonPressed() && m_IsLaunchActivated)
            {
                m_mouseEndPos = inputHandler.AimSystem.GetPointerLocation();
                m_mouseReleased = true;
            }

        }



        public void UpdateGameLogic(int deltaTimeMS)
        {
            if (m_Position.X + m_PersonImage[(int)PeopleState.PEOPLE_IDLE].Width < 0)
            {
                m_isAlive = false;
            }

            //if mouse has been released and the delta aim is not zero
            if (m_IsLaunchActivated && m_mouseReleased)
            {
                m_State = PeopleState.PEOPLE_FLYING;
                //m_randRotation = 100.0f + (float)m_Random.Next(900);

                //if(m_Random.Next(100) > 50) {
                //    m_randRotation = -m_randRotation;
                //}
                

                //make launch direction dependable on time instead of length of vector, when compiling for mobile phone
                m_launchDirection = (m_mouseEndPos - m_mouseStartPos) * 0.5f;
                m_IsLaunchActivated = false;

                const float MAX_LIMIT = 47.0f;
                if (m_launchDirection.X > 0)
                {
                    if (m_launchDirection.X > MAX_LIMIT)
                    {
                        m_launchDirection.X = MAX_LIMIT;
                    }
                } else {
                    if (m_launchDirection.X < -MAX_LIMIT)
                    {
                        m_launchDirection.X = -MAX_LIMIT;
                    }
                }

                if (m_launchDirection.Y > 0)
                {
                    if (m_launchDirection.Y > MAX_LIMIT)
                    {
                        m_launchDirection.Y = MAX_LIMIT;
                    }
                }
                else
                {
                    if (m_launchDirection.Y < -MAX_LIMIT)
                    {
                        m_launchDirection.Y = -MAX_LIMIT;
                    }
                }
                
            }

            if(m_State != PeopleState.PEOPLE_FLYING) {
                m_Position.X -= deltaTimeMS * 0.3f;
            }

        }

        public Vector2 GetBounds() {
            return m_Bounds;
        }

        public bool IsAlive()
        {
            return m_isAlive;
        }

        public void Kill() {
            m_isAlive = false;
        }

        public abstract void Spawn();


        public bool IgnoreCollision()
        {
            return m_State == PeopleState.PEOPLE_DEAD;
        }
    }
}
