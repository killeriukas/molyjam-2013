﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace MolyJam.Core.GameScenes.Mesh.People
{
    public class OldGranies : APeople
    {
        //private Vector2 m_defaultPosition;

        private int m_ScreenWidth;

        public OldGranies(ContentManager contentManager, int windowWidth) : base(contentManager) {
            m_PersonImage = new Texture2D[3];
            m_PersonImage[(int)PeopleState.PEOPLE_IDLE] = contentManager.Load<Texture2D>("Mesh/person_standing");
            m_PersonImage[(int)PeopleState.PEOPLE_FLYING] = contentManager.Load<Texture2D>("Mesh/person_flying");
            m_PersonImage[(int)PeopleState.PEOPLE_DEAD] = contentManager.Load<Texture2D>("Mesh/person_dead");

            m_Bounds = new Vector2(m_PersonImage[(int)PeopleState.PEOPLE_IDLE].Width, m_PersonImage[(int)PeopleState.PEOPLE_IDLE].Height);
            //m_PersonOrigin = m_Bounds
            m_Position = new Vector2(windowWidth, 360);
            m_ScreenWidth = windowWidth;
        }

        protected override void Initialize() {
            base.Initialize();
            m_Position = new Vector2(m_ScreenWidth + 50 * m_Random.Next(100), 360);
        }

        public override void Spawn()
        {
            InitializePhysics();
            Initialize();
        }
    }
}
