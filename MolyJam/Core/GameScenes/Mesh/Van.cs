﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MolyJam.Core.GameScenes.Mesh
{
    public class Van
    {
        private Vector2 m_Position;
        private Texture2D[] m_VanImage;
        private int m_vanCount;
        private int m_SpriteChangeTime;
        private int m_SpriteNumber;

        public Van(float y, Texture2D[] vanImage, int vanNumber) {
            m_Position = new Vector2(-vanImage[0].Width, y);
            m_VanImage = vanImage;
            m_vanCount = vanNumber;
            m_SpriteChangeTime = 0;
            m_SpriteNumber = vanNumber % 2;
        }

        public void UpdateLogic(int deltaTimeMS) {
            m_SpriteChangeTime += deltaTimeMS;
        }

        public void MoveBy(float moveBy) {
            m_Position.X = -(m_VanImage[0].Width*m_vanCount) + moveBy;
         }

        public void Render(SpriteBatch spriteBatch)
        {
            m_SpriteNumber %= 2;

            spriteBatch.Draw(m_VanImage[m_SpriteNumber], m_Position, Color.White);

            if (m_SpriteChangeTime > 100)
            {
                m_SpriteChangeTime -= 100;
                ++m_SpriteNumber;
            }
        }

        public void ChangeSpriteSheet(Texture2D[] vanTexture)
        {
            m_VanImage = vanTexture;
        }
    }
}
