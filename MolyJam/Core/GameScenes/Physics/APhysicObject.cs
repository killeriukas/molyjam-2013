﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace MolyJam.Core.GameScenes.Physics
{

    public class APhysicObject
    {
        private enum PhysicsState
        {
            PHYSICS_DISABLED = 0,
            PHYSICS_ENABLED = 1,
        }

        public static float GRAVITY = 9.8f;
        private PhysicsState m_PhysicsState;
        protected Vector2 m_Position;
        protected int m_Mass;
        protected Vector2 m_Velocity;
        protected Vector2 m_Acceleration;
        protected float m_friction;
        protected float m_Rotation;

        public APhysicObject() {
            InitializePhysics();
        }

        protected virtual void InitializePhysics() {
            m_PhysicsState = PhysicsState.PHYSICS_ENABLED;
            m_Position = Vector2.Zero;
            m_Mass = 10;
            m_Velocity = Vector2.Zero;
            m_Acceleration = Vector2.Zero;
            m_friction = 0.4f;
            m_Rotation = 0;
        }

        protected void DisablePhysics()
        {
            m_PhysicsState = PhysicsState.PHYSICS_DISABLED;
        }

        public virtual void UpdatePhysics(int deltaTimeMS)
        {
            //if physics disabled, ignore physics
            if (m_PhysicsState != PhysicsState.PHYSICS_ENABLED)
            {
                return;
            }

            //add physics stuff
            ApplyGravity(deltaTimeMS);
            ApplyAcceleration(deltaTimeMS);
            ApplyFriction(deltaTimeMS);

            //update the physics object position
            m_Position += m_Velocity;
        }

        private void ApplyFriction(int deltaTimeMS)
        {
            m_Velocity -= m_friction * m_Acceleration;
            //if (m_Velocity.X > 0)
            //{
            //    m_Velocity.X += m_friction * m_Acceleration.X;
            //}
            //else
            //{
            //    m_Velocity.X -= m_friction * m_Acceleration.X;
            //}
        }

        private void ApplyAcceleration(int deltaTimeMS)
        {
            m_Velocity += m_Acceleration;
        }

        private void ApplyGravity(int deltaTimeMS)
        {
            //because Y direction goes down in XNA 2D
            m_Acceleration.Y += GRAVITY * deltaTimeMS * m_Mass * 0.00001f;
        }

        protected void ApplyLinearForce(Vector2 acceleration)
        {
            m_Acceleration += acceleration * m_Mass * 0.001f;
        }

        protected void ApplyAngularForce(float acceleration) {
            //this one is just push, no friction, nothing!
            //it is not real, just bare bones
            m_Rotation += acceleration;
        }

        public Vector2 GetPosition() {
            return m_Position;
        }
    }
}
