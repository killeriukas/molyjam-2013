﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MolyJam.Core.GameScenes.Physics
{
    public class PhysicsClass
    {
        private List<APhysicObject> m_PhysicsObjects;

        public PhysicsClass()
        {
            m_PhysicsObjects = new List<APhysicObject>();
        }

        public void Add(APhysicObject objectPhysics)
        {
            m_PhysicsObjects.Add(objectPhysics);
        }

        public void RunSimulation(int deltaTimeMS) {
            for (int i = 0; i < m_PhysicsObjects.Count; ++i)
            {
                m_PhysicsObjects[i].UpdatePhysics(deltaTimeMS);
            }
        }


        public void Clear()
        {
            m_PhysicsObjects.Clear();
        }
    }
}
