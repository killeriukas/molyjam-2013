﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace MolyJam.Core.GameScenes
{
    public class Player
    {

        private SpriteFont m_Font;
        private int m_SavedPeople;
        private int m_DeadBodies;
        private Vector2 m_PositionSave;
        private Vector2 m_PositionDead;

        public Player(ContentManager contentManager) {
            m_Font = contentManager.Load<SpriteFont>("Font/UIFont");
            m_SavedPeople = 0;
            m_DeadBodies = 0;
            m_PositionSave = new Vector2(0, 0);
            m_PositionDead = new Vector2(0, 20);
        }

        public int GetDeadBodies() {
            return m_DeadBodies;
        }

        public int GetSavedSouls() {
            return m_SavedPeople;
        }

        public void IncreaseSavedLifes() {
            ++m_SavedPeople;
        }

        public void IncreaseDeadLifes() {
            ++m_DeadBodies;
        }

        public void Render(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch) {
            spriteBatch.DrawString(m_Font, "Smashed Bodies: " + m_DeadBodies, m_PositionDead, Color.Black);
            spriteBatch.DrawString(m_Font, "Saved Bodies: " + m_SavedPeople + " / 10", m_PositionSave, Color.Black);
        }



    }
}
