﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTS.Framework.SceneManager;
using RTS.Framework.InputControl;
using MolyJam.Core.GameScenes;
using Microsoft.Xna.Framework.Content;
using RTS.Core.Controls;
using MolyJam.Core.Audio;

namespace MolyJam.Core.GameSystem
{
    class GameSystem : MainGame
    {

        public GameSystem()
            : base(800, 500)
        {

            //add new input controls to the system
            //m_SceneManager.ChangeInputHandler(new InputHandler(new MouseInput(), new KeyboardInput()));
            m_SceneManager.ChangeInputHandler(new InputHandler(new MouseInput(), null));

            //add all the scenes into the scene manager
            m_SceneManager.AddScene(new GamePlayScene(m_GraphicsDevice.PreferredBackBufferWidth, m_GraphicsDevice.PreferredBackBufferHeight, new ContentManager(base.Services)));
            m_SceneManager.AddScene(new GameOverScene(m_GraphicsDevice.PreferredBackBufferWidth, m_GraphicsDevice.PreferredBackBufferHeight, new ContentManager(base.Services)));
            m_SceneManager.AddScene(new MainMenuScene(m_GraphicsDevice.PreferredBackBufferWidth, m_GraphicsDevice.PreferredBackBufferHeight, new ContentManager(base.Services)));
            m_SceneManager.AddScene(new GameWinScene(m_GraphicsDevice.PreferredBackBufferWidth, m_GraphicsDevice.PreferredBackBufferHeight, new ContentManager(base.Services)));
            m_SceneManager.ChangeScene("MainMenuScene");

        }

    }
}
