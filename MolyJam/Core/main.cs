﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using MolyJam.Core.GameSystem;
#endregion

namespace MolyJam
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class main
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new GameSystem())
                game.Run();
        }
    }
#endif
}
