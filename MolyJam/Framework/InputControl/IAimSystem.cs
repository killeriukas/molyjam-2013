﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RTS.Framework.InputControl
{
    /*
    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  START												  |
    ===============================================================================================

                                CLASS FOR ABSTRACT AIM SYSTEM

    Interface for the aim system. The main pointers are expressed here, but need to be redefined
    in the child classes. These pointers are only the main ones. It might need to be updated with
    the new ones after a while, however, other classes won't need to be changed.

    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  END												  |
    ===============================================================================================
    */

    public interface IAimSystem
    {
        Vector2 GetPointerLocation();
        Vector2 GetPointerDeltas();
        bool IsMouseLeftButtonPressedOnce();
        bool IsMouseLeftButtonPressed();
        bool IsMouseRightButtonPressedOnce();
        bool IsMouseRightButtonPressed();
        void Update(int deltaTimeMS);
    }
}
