﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTS.Framework.InputControl
{

    /*
    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  START												  |
    ===============================================================================================

                                CLASS FOR ABSTRACT CONTROL SYSTEM

    Interface for the control system. The main buttons are expressed here, but need to be redefined
    in the child classes. These buttons are only the main ones. It might need to be updated with
    the new ones after a while, however, other classes won't need to be changed.

    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  END												  |
    ===============================================================================================
    */

    public interface IControlSystem
    {
        bool IsQuit();
        void Update(float deltaTimeMS);
    }
}
