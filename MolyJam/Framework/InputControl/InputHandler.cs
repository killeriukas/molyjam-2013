﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTS.Framework.InputControl
{

    /*
    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  START												  |
    ===============================================================================================
    Input handler class is responsible for the input of the user. It takes all the information from
    buttons and a pointer and pass it to the programme. It's like a bridge between abstract input
    devices. All the control and aim classes should be inherited from IControlSystem and IAimSystem
    interfaces and passed to this class through the constructor.

    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  END												  |
    ===============================================================================================
    */

    public class InputHandler
    {
        private IAimSystem m_AimSystem;
        private IControlSystem m_ControlSystem;

        public InputHandler(IAimSystem aimSystem, IControlSystem controlSystem) {
            this.m_AimSystem = aimSystem;
            this.m_ControlSystem = controlSystem;
        }

        public void Update(int deltaTimeMS) {
            if(m_AimSystem != null) {
                m_AimSystem.Update(deltaTimeMS);
            }
            if(m_ControlSystem != null) {
                m_ControlSystem.Update(deltaTimeMS);
            }
        }

        public IAimSystem AimSystem { get { return m_AimSystem;  } }
        public IControlSystem ControlSystem { get { return m_ControlSystem; } }


    }
}
