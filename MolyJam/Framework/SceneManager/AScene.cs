﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
namespace RTS.Framework.SceneManager
{

    /*
    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  START												  |
    ===============================================================================================

    This class is a abstract class for the scene classes. Inherit this class if you want to create
    scenes such as main menu, game play, pause, game over scenes.

    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  END												  |
    ===============================================================================================
    */

    public abstract class AScene
    {
        private string m_sceneName;
        private SceneManager m_SceneManager;
        protected ContentManager m_ContentManager;
        protected readonly int WINDOW_WIDTH;
        protected readonly int WINDOW_HEIGHT;

        public AScene(string name, int windowX, int windowY, ContentManager contentManager) {
            this.m_sceneName = name;
            this.m_ContentManager = contentManager;
            contentManager.RootDirectory = "Assets";
            WINDOW_WIDTH = windowX;
            WINDOW_HEIGHT = windowY;
        }

        public string GetSceneName { get { return m_sceneName;  } }
        public void SetSceneManagerPointer(SceneManager sceneManager)
        {
            this.m_SceneManager = sceneManager;
        }

        protected void ChangeScene(string nextScene) {
            m_SceneManager.ChangeScene(nextScene);
        }

        protected void ExitApplication() {
            m_SceneManager.ExitApplication();
        }

        public abstract MyResult Start();
        public abstract void Stop();
        public abstract MyResult UpdateInput(int deltaTimeMS, RTS.Framework.InputControl.InputHandler inputHandler);
        public abstract MyResult UpdateGameLogic(int deltaTimeMS);
        public abstract void Render(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch);
    }
}
