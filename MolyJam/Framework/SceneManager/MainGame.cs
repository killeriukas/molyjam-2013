﻿#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RTS.Framework.InputControl;

#endregion

namespace RTS.Framework.SceneManager
{
    /*
    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  START												  |
    ===============================================================================================

    This class is the main class which needs to be inherited in order to use scene manager features.
    The input handling and other good stuff used for games comes for free. Just override some methods
    and you are good to go.

    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  END												  |
    ===============================================================================================
    */


    public abstract class MainGame : Game
    {
        protected SceneManager m_SceneManager;

        protected GraphicsDeviceManager m_GraphicsDevice;
        private SpriteBatch m_SpriteBatch;

        public MainGame() : base()
        {
            InitializeMainSources();

            //make application full screen
            m_GraphicsDevice.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            m_GraphicsDevice.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            m_GraphicsDevice.IsFullScreen = true;
            
        }

        public MainGame(int width, int height) : base() {
            InitializeMainSources();

            //make application windowed
            m_GraphicsDevice.PreferredBackBufferWidth = width;
            m_GraphicsDevice.PreferredBackBufferHeight = height;
            m_GraphicsDevice.IsFullScreen = false;

        }

        private void InitializeMainSources() {
            //input handler passed to the scene manager with default inputs, i.e. mouse/keyboard
            m_SceneManager = new SceneManager(null);

            //set content directory to Assets
            Content.RootDirectory = "Assets";

            //creates and attach the graphics device to this class
            m_GraphicsDevice = new GraphicsDeviceManager(this);

            //initialize sprite batch
            m_SpriteBatch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //if any of the scenes asked to exit the application, exit it
            if(m_SceneManager.IsExitApplicationActivated()) {
                this.Exit();
            }
               
            //ask for the scene manager to update itself
            m_SceneManager.UpdateInput(gameTime.ElapsedGameTime.Milliseconds);
            m_SceneManager.UpdateGameLogic(gameTime.ElapsedGameTime.Milliseconds);

            base.Update(gameTime);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            m_SceneManager.DeleteMap();
            Content.Unload();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //clear the renderer
            GraphicsDevice.Clear(Color.Black);

            //ask for scene manager to render itself
            m_SceneManager.Render(m_SpriteBatch);

            base.Draw(gameTime);
        }
    }
}
