﻿using System;
using System.Collections.Generic;
using RTS.Framework.InputControl;
using Microsoft.Xna.Framework.Graphics;



namespace RTS.Framework.SceneManager
{

    /*
    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  START												  |
    ===============================================================================================

    This class is a scene manager, which allows you to create main menu, game-play, end game scenes
    and much more.

    ===============================================================================================
    |																							  |
    |									CLASS EXPLANATION										  |
    |										  END												  |
    ===============================================================================================
    */

    //add more return types, when needed
    public enum MyResult {
	    MY_RESULT_WARNING = -1,	
	    MY_RESULT_SUCCESSFUL = 0,
	    MY_RESULT_CRITICAL_ERROR = 1,
	    MY_RESULT_NOT_ENOUGH_MEMORY = 2
    }


    public class SceneManager
    {
        Dictionary <string,AScene> m_SceneMap;
	    //std::map<std::string,IEvent*> m_EventMap;
	    string m_activeScene;
	   // string m_pauseScene;
	    string m_waitingScene;
	    bool m_exitActivated;
	    InputHandler m_InputHandler;

        public SceneManager(InputHandler inputHandler) {
            m_SceneMap = new Dictionary<string, AScene>();
          	//m_pauseScene = "";
            m_activeScene = null;
	        m_waitingScene = null;
	        m_InputHandler = inputHandler;
	        m_exitActivated = false;  
        }

        public void AddScene(AScene sceneObject) {
           //before adding a scene into the array, SceneManager sets this scene inputhandler and pointer of manager
	        sceneObject.SetSceneManagerPointer(this);
	        //sceneObject.SetInputHandler(m_InputHandler);
	        m_SceneMap.Add(sceneObject.GetSceneName,sceneObject);
        }

        public void ChangeScene(string nextScene) {
            MyResult result = m_SceneMap[nextScene].Start();
            if (result != MyResult.MY_RESULT_SUCCESSFUL)
            {
                Console.WriteLine("SceneManager was unable to start the scene in ChangeScene method!");
            }

            //if active scene is empty, start the scene instantly
            if (m_activeScene == null)
            {
                m_activeScene = nextScene;
            }
            else
            {
                //sets it into pending position
                m_waitingScene = nextScene;
            }
                
        }

        public void ExitApplication() {
            m_exitActivated = true;
        }

        public bool IsExitApplicationActivated() {
            return m_exitActivated;
        }

        public MyResult UpdateInput(int deltaTimeMS)
        {
            m_InputHandler.Update(deltaTimeMS);
            if (m_activeScene == null) {
                return MyResult.MY_RESULT_SUCCESSFUL;
            }
            //check whether there is a need to change a scene before even updating the input
            CheckWaitingSceneStatus();
            MyResult result = m_SceneMap[m_activeScene].UpdateInput(deltaTimeMS, m_InputHandler);
	        return result;
        }

        public MyResult UpdateGameLogic(int deltaTimeMS)
        {
            if (m_activeScene == null)
            {
                return MyResult.MY_RESULT_SUCCESSFUL;
            }

           	MyResult result = m_SceneMap[m_activeScene].UpdateGameLogic(deltaTimeMS);
	        return result;
        }

        public void Render(SpriteBatch spriteBatch) {
            if (m_activeScene == null) {
                return;
            }
            m_SceneMap[m_activeScene].Render(spriteBatch);
        }

	    private void CheckWaitingSceneStatus() {
	        //checks whether there is a pending scene
	        if(m_waitingScene != null) {

		        //if we got a pending scene, we shutdown this scene
		        //and change active scene to the pending one
		        //change pending screen into NULL
		        m_SceneMap[m_activeScene].Stop();
		        m_activeScene = m_waitingScene;
		        m_waitingScene = null;
	        }
        }

	    //private void PauseScene(string);
	    //private void ResumeScene();

	    public void DeleteMap() {
          //  foreach (KeyValuePair<string, AScene> pair in m_SceneMap)
           // {
          //      pair.Value.Stop();
          //  }
            m_SceneMap[m_activeScene].Stop();
            m_SceneMap.Clear();
        }

        public void ChangeInputHandler(InputHandler inputHandler) {
            this.m_InputHandler = inputHandler;
        }

    }
}
